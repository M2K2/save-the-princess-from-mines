﻿using System;

namespace Save_The_Princess_From_Mines
{
    class Game
    {
        public char[,] Field;
        public int
        Size,
        plX,
        plY,
        Mines;


        public Game(int Size, int Mines)
        {
            this.Size = Size;
            plX = 1;
            plY = 1;
            this.Mines = Mines;
            GenerateField();
            DrawField();
        }

        private void GenerateField()
        {
            Field = new char[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                Field[0, i] = '-';
                Field[i, 0] = '|';
                Field[Size - 1, i] = '-';
                Field[i, Size - 1] = '|';
            }
            for (int i = ' '; i < Size - 1; i++)
            {
                for (int j = ' '; j < Size - 1; j++)
                {
                    Field[i, j] = ' ';
                }
            }
            Random r = new Random();
            for (int i = 0; i < Mines; i++)
            {
                int
                mineX = r.Next(2, Size - 1),
                mineY = r.Next(2, Size - 1);
                Field[mineX, mineY] = 'Ж';
            }
            Field[Size - 2, Size - 2] = '8';
            Field[plX, plY] = '@';
        }

        private void DrawField()
        {
            Console.Clear();
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Console.Write($"{Field[i, j]} ");
                }
                Console.WriteLine();
            }
        }

        public void Play()
        {
            while (true)
            {
                ConsoleKeyInfo cki = Console.ReadKey();
                bool death = false;
                switch (cki.Key)
                {
                    case ConsoleKey.S:
                        {
                            if (Field[plX + 1, plY] != '-')
                            {
                                if (Field[plX + 1, plY] == 'Ж')
                                {
                                    death = true;
                                }
                                Field[plX, plY] = ' ';
                                Field[plX + 1, plY] = '@';
                                plX++;
                            }
                            break;
                        }
                    case ConsoleKey.W:
                        {
                            if (Field[plX - 1, plY] != '-')
                            {
                                if (Field[plX - 1, plY] == 'Ж')
                                {
                                    death = true;
                                }
                                Field[plX, plY] = ' ';
                                Field[plX - 1, plY] = '@';
                                plX--;
                            }
                            break;
                        }
                    case ConsoleKey.A:
                        {
                            if (Field[plX, plY - 1] != '|')
                            {
                                if (Field[plX, plY - 1] == 'Ж')
                                {
                                    death = true;
                                }
                                Field[plX, plY] = ' ';
                                Field[plX, plY - 1] = '@';
                                plY--;
                            }
                            break;
                        }
                    case ConsoleKey.D:
                        {
                            if (Field[plX, plY + 1] != '|')
                            {
                                if (Field[plX, plY + 1] == 'Ж')
                                {
                                    death = true;
                                }
                                Field[plX, plY] = ' ';
                                Field[plX, plY + 1] = '@';
                                plY++;
                            }
                            break;
                        }
                }
                DrawField();
                if (plX == Size - 2 && plY == Size - 2)
                {
                    Console.WriteLine("ТЫ СПАС ПРИНЦЕССУ!");
                    break;
                }
                if (death)
                {
                    Field[plX, plY] = 'X';
                    DrawField();
                    Console.WriteLine("ТЫ ВЗОРВАЛСЯ!");
                    break;
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(20, 100);
            game.Play();
            Console.ReadLine();
        }
    }
}